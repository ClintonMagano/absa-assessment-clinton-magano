package com.psybergate.absa.assessment1.scenario1.service.impl;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.psybergate.absa.assessment1.scenario1.entity.Transaction;
import com.psybergate.absa.assessment1.scenario1.entity.UsageRecord;
import com.psybergate.absa.assessment1.scenario1.repository.TransactionRepository;
import com.psybergate.absa.assessment1.scenario1.repository.UsageRecordRepository;
import com.psybergate.absa.assessment1.scenario1.util.billingfilewriter.BillingFileWriter;
import com.psybergate.absa.assessment1.scenario1.util.fileuploader.FileUploader;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UsageRecordExtractorServiceImplTest {

	@InjectMocks
	private UsageRecordExtractorServiceImpl service;

	@Mock
	private TransactionRepository transactionRepository;

	@Mock
	private UsageRecordRepository usageRecordRepository;
	
	@Mock
	private BillingFileWriter billingFileWriter;

	@Mock
	private FileUploader fileUploader;

	@Test
	public void sendUsageRecordsDuringPeriod_CallsBillingFileWriterWithCorrectUsageRecords() {
		LocalDate now = LocalDate.now();
		LocalDate firstDay = now.minusMonths(1).withDayOfMonth(1);
		LocalDate lastDay = now.withDayOfMonth(1).minusDays(1);
		List<Transaction> transactions = getCompletedTestTransactions();
		Set<UsageRecord> expectedRecords = getExpectedUsageRecords();
		String fileName = "billing_file.txt";

		when(transactionRepository.findByMessageStatusAndDateCreatedGreaterThanEqualAndDateCreatedLessThanEqual(
			"Completed", firstDay, lastDay)).thenReturn(transactions);

		service.sendUsageRecordsDuringPeriod(firstDay, lastDay);

		verify(fileUploader).uploadFileToBilling(fileName);
		ArgumentCaptor<Set<UsageRecord>> usageRecordsCaptor = ArgumentCaptor.forClass(Set.class);
		verify(billingFileWriter).writeToBillingFile(eq(fileName), usageRecordsCaptor.capture());
		assertEquals(expectedRecords, usageRecordsCaptor.getValue());
	}

	@Test
	public void sendUsageRecordsDuringPeriod_UsageRecordRepositoryToSaveUsageRecords() {
		LocalDate now = LocalDate.now();
		LocalDate firstDay = now.minusMonths(1).withDayOfMonth(1);
		LocalDate lastDay = now.withDayOfMonth(1).minusDays(1);
		List<Transaction> transactions = getCompletedTestTransactions();
		Set<UsageRecord> expectedRecords = getExpectedUsageRecords();
		
		when(transactionRepository.findByMessageStatusAndDateCreatedGreaterThanEqualAndDateCreatedLessThanEqual(
			"Completed", firstDay, lastDay)).thenReturn(transactions);
		
		service.sendUsageRecordsDuringPeriod(firstDay, lastDay);
		
		ArgumentCaptor<Set<UsageRecord>> usageRecordCaptor = ArgumentCaptor.forClass(Set.class);
		verify(usageRecordRepository).saveAll(usageRecordCaptor.capture());
		assertEquals(expectedRecords, usageRecordCaptor.getValue());
	}

	@Test
	public void getUsageRecords_ReturnsUsageRecordsForTransactions_WhenGivenTransactions() {
		List<Transaction> transactions = getCompletedTestTransactions();
		Set<UsageRecord> expected = getExpectedUsageRecords();

		Set<UsageRecord> result = service.getUsageRecords(transactions);

		assertEquals(expected, result);
	}

	/**
	 * Creates 18 transactions split this way:<br>
	 * Creates 6 completed domestic transactions, 6 completed international
	 * transactions; 3 Rejected domestic transactions, 3 Rejected international
	 * transactions.
	 * <p>
	 * Of the 6 completed domestic transactions:<br>
	 * 3 have the same swift address, the rest have different addresses to all
	 * others.
	 * <p>
	 * Of the 6 completed international transactions:<br>
	 * 3 have the same swift address, the rest have different addresses to all
	 * others.
	 * 
	 * @return a list of transactions
	 */
	private List<Transaction> getTestTransactions() {
		LocalDate now = LocalDate.now();
		BigDecimal amount = BigDecimal.valueOf(1000.00);
		List<Transaction> transactions = new ArrayList<>();
		// 3 Completed domestic transactions with different swift addresses, and a
		// rejected transaction for each address
		for (int i = 1; i <= 3; i++) {
			long id = Long.valueOf(-i);
			String completedRef = String.format("COMPLETEDREF%04d", i);
			String rejectedRef = String.format("REJECTEDREFX%04d", i);
			String clientSwiftAddress = String.format("ABSAZAJJ%04d", i);
			transactions.add(new Transaction(id, completedRef, clientSwiftAddress, "Completed", "ZAR", amount, now));
			transactions.add(new Transaction(id, rejectedRef, clientSwiftAddress, "Rejected", "ZAR", amount, now));
		}
		// 3 Completed domestic transactions with with the same swift addresses
		for (int i = 4; i <= 6; i++) {
			long id = Long.valueOf(-i);
			String transactionRef = String.format("TRANSACTIONREF%02d", i);
			String clientSwiftAddress = String.format("ABSAZAJJSAME", i);
			transactions.add(new Transaction(id, transactionRef, clientSwiftAddress, "Completed", "ZAR", amount, now));
		}
		// 3 Completed international transactions with different swift addresses, and a
		// rejected transaction for each address
		for (int i = 7; i <= 9; i++) {
			long id = Long.valueOf(-i);
			String completedRef = String.format("COMPLETEDREF%04d", i);
			String rejectedRef = String.format("REJECTEDREFX%04d", i);
			String clientSwiftAddress = String.format("BOFAUS3N%04d", i);
			transactions.add(new Transaction(id, completedRef, clientSwiftAddress, "Completed", "USD", amount, now));
			transactions.add(new Transaction(id, rejectedRef, clientSwiftAddress, "Rejected", "USD", amount, now));
		}
		// 3 Completed international transactions with with the same swift addresses
		for (int i = 10; i <= 12; i++) {
			long id = Long.valueOf(-i);
			String transactionRef = String.format("TRANSACTIONREF%02d", i);
			String clientSwiftAddress = String.format("BOFAUS3NSAME", i);
			transactions.add(new Transaction(id, transactionRef, clientSwiftAddress, "Completed", "USD", amount, now));
		}

		return transactions;
	}

	private List<Transaction> getCompletedTestTransactions() {
		return getTestTransactions().stream().filter(t -> t.getMessageStatus().equals("Completed"))
				.collect(Collectors.toList());
	}

	private Set<UsageRecord> getExpectedUsageRecords() {
		Set<UsageRecord> expectedUsageRecords = new HashSet<>();
		LocalDate now = LocalDate.now();
		expectedUsageRecords.add(new UsageRecord("INTEGRATEDSERVICES", "ABSAZAJJ0001", "ZAROUT", now, 1));
		expectedUsageRecords.add(new UsageRecord("INTEGRATEDSERVICES", "ABSAZAJJ0002", "ZAROUT", now, 1));
		expectedUsageRecords.add(new UsageRecord("INTEGRATEDSERVICES", "ABSAZAJJ0003", "ZAROUT", now, 1));
		expectedUsageRecords.add(new UsageRecord("INTEGRATEDSERVICES", "ABSAZAJJSAME", "ZAROUT", now, 3));
		expectedUsageRecords.add(new UsageRecord("INTEGRATEDSERVICES", "BOFAUS3N0007", "CCYOUT", now, 1));
		expectedUsageRecords.add(new UsageRecord("INTEGRATEDSERVICES", "BOFAUS3N0008", "CCYOUT", now, 1));
		expectedUsageRecords.add(new UsageRecord("INTEGRATEDSERVICES", "BOFAUS3N0009", "CCYOUT", now, 1));
		expectedUsageRecords.add(new UsageRecord("INTEGRATEDSERVICES", "BOFAUS3NSAME", "CCYOUT", now, 3));

		return expectedUsageRecords;
	}

}
