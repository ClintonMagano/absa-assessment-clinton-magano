package com.psybergate.absa.assessment1.scenario1.scheduler;

import java.time.LocalDate;

import com.psybergate.absa.assessment1.scenario1.service.UsageRecordExtractorService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class UsageRecordRetrieverTaskSchedulerTest {

	@InjectMocks
	private UsageRecordRetrieverTaskScheduler monthlyUsageTaskScheduler;

	@Mock
	UsageRecordExtractorService usageRecordExtractorService;

	@Test
	public void sendMonthlyUsageStatsToBillingApplication_CallsUsageRecordWithPreviousMonthStartAndEndDates() {
		// The first business day of the month can either be the 1st, 2nd or 3rd day of
		// the month.
		LocalDate now = LocalDate.now();
		LocalDate previousMonthFirstDay = now.minusMonths(1).withDayOfMonth(1);
		LocalDate previousMonthLastDay = now.withDayOfMonth(1).minusDays(1);
		monthlyUsageTaskScheduler.sendMonthlyUsageStatsToBillingApplication();

		ArgumentCaptor<LocalDate> startDateCaptor = ArgumentCaptor.forClass(LocalDate.class);
		ArgumentCaptor<LocalDate> endDateCaptor = ArgumentCaptor.forClass(LocalDate.class);
		verify(usageRecordExtractorService).sendUsageRecordsDuringPeriod(startDateCaptor.capture(),
			endDateCaptor.capture());
		assertEquals(previousMonthFirstDay, startDateCaptor.getValue());
		assertEquals(previousMonthLastDay, endDateCaptor.getValue());
	}

}
