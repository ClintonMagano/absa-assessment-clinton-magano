package com.psybergate.absa.assessment1.scenario1.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TransactionTest {

	@Test
	public void equals_ReturnsTrueForTransactionsWithTheSameTransactionReference() {
		Long id = -1L;
		String transactionReference = "REF";
		BigDecimal amount = BigDecimal.valueOf(1234.56d);
		LocalDate dateTimeCreated = LocalDate.of(2019, 12, 10);
		Transaction transaction1 = new Transaction(id, transactionReference, "", "", "", amount, dateTimeCreated);
		Transaction transaction2 = new Transaction(id, transactionReference, "", "", "", amount, dateTimeCreated);

		boolean result = transaction1.equals(transaction2);

		assertTrue(result);
	}

	@Test
	public void equals_ReturnsFalseForTransactionsWithDifferentTransactionReferences() {
		Long id = -1L;
		BigDecimal amount = BigDecimal.valueOf(1234.56d);
		LocalDate dateTimeCreated = LocalDate.of(2019, 12, 10);
		Transaction transaction1 = new Transaction(id, "REF1", "", "", "", amount, dateTimeCreated);
		Transaction transaction2 = new Transaction(id, "REF2", "", "", "", amount, dateTimeCreated);

		boolean result = transaction1.equals(transaction2);

		assertFalse(result);
	}

}
