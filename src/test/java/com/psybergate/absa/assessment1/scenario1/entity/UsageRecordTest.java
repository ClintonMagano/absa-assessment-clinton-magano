package com.psybergate.absa.assessment1.scenario1.entity;

import java.time.LocalDate;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class UsageRecordTest {

	@Test
	public void equals_ReturnsTrue_WhenGivenUsageRecordWithTheSameSwiftAddressAndSubServiceAndDateCreated() {
		LocalDate dateCreated = LocalDate.of(2019, 12, 10);
		UsageRecord record1 = new UsageRecord(null, "ABSAZAJJ0003", "ZAROUT", dateCreated, null);
		UsageRecord record2 = new UsageRecord(null, "ABSAZAJJ0003", "ZAROUT", dateCreated, null);

		boolean result = record1.equals(record2);

		assertTrue(result);
	}

	@Test
	public void equals_ReturnsFalse_WhenGivenUsageRecordWithDifferentSubService() {
		LocalDate dateCreated = LocalDate.of(2019, 12, 10);
		UsageRecord record1 = new UsageRecord(null, "ABSAZAJJ0003", "ZAROUT", dateCreated, null);
		UsageRecord record2 = new UsageRecord(null, "ABSAZAJJ0003", "CCYOUT", dateCreated, null);

		boolean result = record1.equals(record2);

		assertFalse(result);
	}

	@Test
	public void equals_ReturnsFalse_WhenGivenUsageRecordWithDifferentSwiftAddress() {
		LocalDate dateCreated = LocalDate.of(2019, 12, 10);
		UsageRecord record1 = new UsageRecord(null, "ABSAZAJJ0003", "ZAROUT", dateCreated, null);
		UsageRecord record2 = new UsageRecord(null, "BOFAUS3N0009", "ZAROUT", dateCreated, null);

		boolean result = record1.equals(record2);

		assertFalse(result);
	}
	
	@Test
	public void equals_ReturnsFalse_WhenGivenUsageRecordWithDifferentDateCreated() {
		LocalDate dateCreated1 = LocalDate.of(2019, 12, 10);
		LocalDate dateCreated2 = LocalDate.of(2019, 12, 20);
		UsageRecord record1 = new UsageRecord(null, "ABSAZAJJ0003", "ZAROUT", dateCreated1, null);
		UsageRecord record2 = new UsageRecord(null, "ABSAZAJJ0003", "ZAROUT", dateCreated2, null);
		
		boolean result = record1.equals(record2);
		
		assertFalse(result);
	}

	@Test
	public void getBillingFileEntry_ReturnsBillingFileEntry() {
		LocalDate dateCreated = LocalDate.of(2019, 12, 10);
		UsageRecord record = new UsageRecord("INTEGRATEDSERVICES", "ABSAZAJJ0003", "ZAROUT", dateCreated, 3);
		String expected = "INTEGRATEDSERVICESABSAZAJJ0003ZAROUT201912100000003";

		String result = record.getBillingFileEntry();

		assertEquals(expected, result);
	}

}
