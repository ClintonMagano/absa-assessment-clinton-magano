CREATE TABLE transaction (
	id SERIAL PRIMARY KEY,
	transactionReference VARCHAR(16) NOT NULL,
	clientSwiftAddress VARCHAR(12) NOT NULL,
	messageStatus VARCHAR(9) NOT NULL,
	currency VARCHAR(3) NOT NULL,
	amount NUMERIC NOT NULL,
	dateTimeCreated DATE NOT NULL);
	
CREATE TABLE usageRecord (
	id SERIAL PRIMARY KEY,
	serviceName VARCHAR(18) NOT NULL,
	clientSwiftAddress VARCHAR(12) NOT NULL,
	subService VARCHAR(6) NOT NULL,
	dateCreated DATE NOT NULL,
	usageStats INT NOT NULL);

INSERT INTO transaction (transactionReference, clientSwiftAddress, messageStatus, currency, amount, dateTimeCreated)
	VALUES
		('transactionRef01', 'ABSAZAJJ0001', 'Completed', 'ZAR', '1093.98', '2019-11-02'),
		('transactionRef02', 'ABSAZAJJ0001', 'Rejected', 'ZAR', '34901.98', '2019-11-04'),
		('transactionRef03', 'ABSAZAJJ0002', 'Completed', 'ZAR', '120.98', '2019-11-19'),
		('transactionRef04', 'BOFAUS3N0001', 'Rejected', 'USD', '90901.98', '2019-11-30'),
		('transactionRef03', 'BOFAUS3N0002', 'Completed', 'USD', '120.98', '2019-11-19');