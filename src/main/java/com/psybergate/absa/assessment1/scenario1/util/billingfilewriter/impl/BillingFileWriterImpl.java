package com.psybergate.absa.assessment1.scenario1.util.billingfilewriter.impl;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.psybergate.absa.assessment1.scenario1.entity.UsageRecord;
import com.psybergate.absa.assessment1.scenario1.util.billingfilewriter.BillingFileWriter;
import org.springframework.stereotype.Component;

@Component
public class BillingFileWriterImpl implements BillingFileWriter {

	@Override
	public void writeToBillingFile(String fileName, Set<UsageRecord> usageRecords) {
		try {
			Path billingFilePath = Paths.get(fileName);
			
			List<String> lines = new ArrayList<>();
			for (UsageRecord usageRecord : usageRecords) {
				lines.add(usageRecord.getBillingFileEntry());
			}
			
			Files.write(billingFilePath, lines, StandardCharsets.UTF_8, StandardOpenOption.CREATE);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
