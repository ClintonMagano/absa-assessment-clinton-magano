package com.psybergate.absa.assessment1.scenario1.entity;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * This represents a single entry in the Billing File sent to Billing.
 */
@Entity
public class UsageRecord {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String serviceName;

	private String clientSwiftAddress;

	private String subService;

	private LocalDate dateCreated;

	private Integer usageStats;

	public UsageRecord(String serviceName, String clientSwiftAddress, String subService, LocalDate dateCreated,
		Integer usageStats) {
		this.serviceName = serviceName;
		this.clientSwiftAddress = clientSwiftAddress;
		this.subService = subService;
		this.dateCreated = dateCreated;
		this.usageStats = usageStats;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getClientSwiftAddress() {
		return clientSwiftAddress;
	}

	public void setClientSwiftAddress(String clientSwiftAddress) {
		this.clientSwiftAddress = clientSwiftAddress;
	}

	public String getSubService() {
		return subService;
	}

	public void setSubService(String subService) {
		this.subService = subService;
	}

	public LocalDate getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(LocalDate dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Integer getUsageStats() {
		return usageStats;
	}

	public void setUsageStats(Integer usageStats) {
		this.usageStats = usageStats;
	}

	public String getBillingFileEntry() {
		String billingFileEntryFormat = "%s%s%s%s%07d";
		String formattedDate = getDateCreated().format(DateTimeFormatter.BASIC_ISO_DATE);
		return String.format(billingFileEntryFormat, serviceName, clientSwiftAddress, subService, formattedDate,
			usageStats);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clientSwiftAddress == null) ? 0 : clientSwiftAddress.hashCode());
		result = prime * result + ((dateCreated == null) ? 0 : dateCreated.hashCode());
		result = prime * result + ((subService == null) ? 0 : subService.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UsageRecord other = (UsageRecord) obj;
		if (clientSwiftAddress == null) {
			if (other.clientSwiftAddress != null)
				return false;
		} else if (!clientSwiftAddress.equals(other.clientSwiftAddress))
			return false;
		if (dateCreated == null) {
			if (other.dateCreated != null)
				return false;
		} else if (!dateCreated.equals(other.dateCreated))
			return false;
		if (subService == null) {
			if (other.subService != null)
				return false;
		} else if (!subService.equals(other.subService))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UsageRecord [id=" + id + ", serviceName=" + serviceName + ", clientSwiftAddress=" + clientSwiftAddress
				+ ", subService=" + subService + ", dateCreated=" + dateCreated + ", usageStats=" + usageStats + "]\n";
	}
	
}
