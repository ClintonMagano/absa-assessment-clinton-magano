package com.psybergate.absa.assessment1.scenario1.scheduler;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.psybergate.absa.assessment1.scenario1.service.UsageRecordExtractorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class UsageRecordRetrieverTaskScheduler {

	/**
	 * This is used to prevent the scheduler running more than once a month.
	 */
	private boolean hasRun = false;

	private UsageRecordExtractorService usageRecordExtractorService;

	@Autowired
	public UsageRecordRetrieverTaskScheduler(UsageRecordExtractorService usageRecordExtractorService) {
		this.usageRecordExtractorService = usageRecordExtractorService;
	}

	/**
	 * Gets the first and last day of the previous month and makes a call to send
	 * usage stats of all successful transactions during that period to Billing.
	 */
	@Scheduled(cron = "${cron.monthlyusagestats}")
	public void sendMonthlyUsageStatsToBillingApplication() {
		if (!hasRun) {
			LocalDate now = LocalDate.now();
			LocalDate previousMonthFirstDay = now.minusMonths(1).withDayOfMonth(1);
			LocalDate previousMonthLastDay = now.withDayOfMonth(1).minusDays(1);
			usageRecordExtractorService.sendUsageRecordsDuringPeriod(previousMonthFirstDay, previousMonthLastDay);
			System.out.println("Billing File Sent");
			hasRun = true;
		}
	}

	/**
	 * Resets the hasRun object variable when a new month is reached. It should be
	 * called before sendMonthlyUsageStatsToBillingApplication() is called.
	 */
	@Scheduled(cron = "0 0 0 1 * ?")
	public void resetHasRun() {
		hasRun = false;
	}
}
