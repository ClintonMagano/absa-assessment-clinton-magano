package com.psybergate.absa.assessment1.scenario1.web.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/billing")
public class BillingFileUploadController {

	@RequestMapping(value = "/sendMonthlyUsageRecords", method = RequestMethod.POST)
	public String receiveBillingFile(@RequestParam MultipartFile billingFile) {
		try {
			InputStream is = billingFile.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String line;
			System.out.println("Billing received the following:");
			while ((line = br.readLine()) != null) {
				System.out.println(line);
			}
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}

		return "File received";
	}

}
