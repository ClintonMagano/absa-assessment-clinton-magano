package com.psybergate.absa.assessment1.scenario1.entity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Represents the data retrieved from the database. It's the same data that
 * would have been retrieved using the 1000+ line SQL script.
 */
@Entity
public class Transaction {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String transactionReference;

	private String clientSwiftAddress;

	private String messageStatus;

	private String currency;

	private BigDecimal amount;

	@Column(name = "dateTimeCreated")
	private LocalDate dateCreated;

	public Transaction() {
	}
	
	public Transaction(Long id, String transactionReference, String clientSwiftAddress, String messageStatus,
			String currency, BigDecimal amount, LocalDate dateCreated) {
		this.id = id;
		this.transactionReference = transactionReference;
		this.clientSwiftAddress = clientSwiftAddress;
		this.messageStatus = messageStatus;
		this.currency = currency;
		this.amount = amount;
		this.dateCreated = dateCreated;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTransactionReference() {
		return transactionReference;
	}

	public void setTransactionReference(String transactionReference) {
		this.transactionReference = transactionReference;
	}

	public String getClientSwiftAddress() {
		return clientSwiftAddress;
	}

	public void setClientSwiftAddress(String clientSwiftAddress) {
		this.clientSwiftAddress = clientSwiftAddress;
	}

	public String getMessageStatus() {
		return messageStatus;
	}

	public void setMessageStatus(String messageStatus) {
		this.messageStatus = messageStatus;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public LocalDate getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(LocalDate dateCreated) {
		this.dateCreated = dateCreated;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Transaction other = (Transaction) obj;
		if (transactionReference == null) {
			if (other.transactionReference != null)
				return false;
		} else if (!transactionReference.equals(other.transactionReference))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Transaction [id=" + id + ", transactionReference=" + transactionReference + ", clientSwiftAddress="
				+ clientSwiftAddress + ", messageStatus=" + messageStatus + ", currency=" + currency + ", amount="
				+ amount + ", dateTimeCreated=" + dateCreated + "]\n";
	}

}
