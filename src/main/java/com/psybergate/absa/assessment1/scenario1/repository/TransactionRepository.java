package com.psybergate.absa.assessment1.scenario1.repository;

import java.time.LocalDate;
import java.util.List;

import com.psybergate.absa.assessment1.scenario1.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

	List<Transaction> findByMessageStatusAndDateCreatedGreaterThanEqualAndDateCreatedLessThanEqual(String messageStatus,
		LocalDate startDate, LocalDate endDate);

}
