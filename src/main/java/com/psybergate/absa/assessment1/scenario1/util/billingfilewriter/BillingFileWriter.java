package com.psybergate.absa.assessment1.scenario1.util.billingfilewriter;

import java.util.Set;

import com.psybergate.absa.assessment1.scenario1.entity.UsageRecord;

public interface BillingFileWriter {
	
	void writeToBillingFile(String fileName, Set<UsageRecord> usageRecords);
	
}
