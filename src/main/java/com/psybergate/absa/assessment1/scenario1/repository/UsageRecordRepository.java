package com.psybergate.absa.assessment1.scenario1.repository;

import com.psybergate.absa.assessment1.scenario1.entity.UsageRecord;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsageRecordRepository extends JpaRepository<UsageRecord, Long> {

}
