package com.psybergate.absa.assessment1.scenario1.util.fileuploader.impl;

import java.io.File;
import java.io.IOException;

import com.psybergate.absa.assessment1.scenario1.util.fileuploader.FileUploader;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class FileUploaderImpl implements FileUploader {

	@Value("${billing.billingfileuploadurl}")
	private String billingFileUploadUrl;

	@Override
	public void uploadFileToBilling(String pathToFile) {
		File billingFile = new File(pathToFile);

		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
			// Create form data
			HttpEntity postData = MultipartEntityBuilder.create().addBinaryBody("billingFile", billingFile).build();
			// Add form data to request
			HttpUriRequest postRequest = RequestBuilder.post(billingFileUploadUrl).setEntity(postData).build();

			HttpResponse response = httpClient.execute(postRequest);

			if (response.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code: " + response.getStatusLine().getStatusCode());
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public String getBillingFileUploadUrl() {
		return billingFileUploadUrl;
	}

	public void setBillingFileUploadUrl(String billingFileUploadUrl) {
		this.billingFileUploadUrl = billingFileUploadUrl;
	}

}
