package com.psybergate.absa.assessment1.scenario1.service.impl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.psybergate.absa.assessment1.scenario1.entity.Transaction;
import com.psybergate.absa.assessment1.scenario1.entity.UsageRecord;
import com.psybergate.absa.assessment1.scenario1.repository.TransactionRepository;
import com.psybergate.absa.assessment1.scenario1.repository.UsageRecordRepository;
import com.psybergate.absa.assessment1.scenario1.service.UsageRecordExtractorService;
import com.psybergate.absa.assessment1.scenario1.util.billingfilewriter.BillingFileWriter;
import com.psybergate.absa.assessment1.scenario1.util.fileuploader.FileUploader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsageRecordExtractorServiceImpl implements UsageRecordExtractorService {

	private static final String SERVICE_NAME = "INTEGRATEDSERVICES";

	private TransactionRepository transactionRepository;

	private UsageRecordRepository usageRecordRepository;

	private BillingFileWriter billingFileWriter;

	private FileUploader fileUploader;

	@Autowired
	public UsageRecordExtractorServiceImpl(TransactionRepository transactionRepository,
		UsageRecordRepository usageRecordRepository, BillingFileWriter billingFileWriter, FileUploader fileUploader) {
		this.transactionRepository = transactionRepository;
		this.usageRecordRepository = usageRecordRepository;
		this.billingFileWriter = billingFileWriter;
		this.fileUploader = fileUploader;
	}

	@Override
	public void sendUsageRecordsDuringPeriod(LocalDate startDate, LocalDate endDate) {
		// Get completed transactions from the previous month.
		List<Transaction> transactions = transactionRepository
			.findByMessageStatusAndDateCreatedGreaterThanEqualAndDateCreatedLessThanEqual("Completed", startDate,
				endDate);

		// Create usage records that summarize completed transactions.
		Set<UsageRecord> usageRecords = getUsageRecords(transactions);

		String fileName = "billing_file.txt";
		billingFileWriter.writeToBillingFile(fileName, usageRecords);

		fileUploader.uploadFileToBilling(fileName);
		
		usageRecordRepository.saveAll(usageRecords);
	}

	@Override
	public Set<UsageRecord> getUsageRecords(List<Transaction> transactions) {
		// Count the number of domestic and international payments for each
		// clientSwiftAddress.

		/*
		 * Format of usagePerSwiftAddress: { "<clientSwiftAddress>": Integer[]{count of
		 * ZAROUT, count of CCYOUT} }
		 */
		Map<String, Integer[]> usagePerSwiftAddress = new HashMap<>();

		int domesticTransactionIndex = 0;
		int internationalTransactionIndex = 1;
		for (Transaction transaction : transactions) {
			String clientSwiftAddress = transaction.getClientSwiftAddress();
			if (!usagePerSwiftAddress.containsKey(clientSwiftAddress)) {
				usagePerSwiftAddress.put(clientSwiftAddress, new Integer[] { 0, 0 });
			}

			if ("ZAR".equals(transaction.getCurrency())) {
				usagePerSwiftAddress.get(clientSwiftAddress)[domesticTransactionIndex]++;
			} else {
				usagePerSwiftAddress.get(clientSwiftAddress)[internationalTransactionIndex]++;
			}
		}

		// Create billing file entries.

		Set<UsageRecord> usageRecords = new HashSet<>();

		LocalDate today = LocalDate.now();
		for (Map.Entry<String, Integer[]> usageRecord : usagePerSwiftAddress.entrySet()) {
			String swiftAddress = usageRecord.getKey();
			String subService = "";

			// ZAROUT entry for swift address
			Integer countOfZarOut = usageRecord.getValue()[domesticTransactionIndex];
			if (!countOfZarOut.equals(0)) {
				subService = "ZAROUT";
				usageRecords.add(new UsageRecord(SERVICE_NAME, swiftAddress, subService, today, countOfZarOut));
			}

			// CCYOUT entry for swift address
			Integer countOfCcyOut = usageRecord.getValue()[internationalTransactionIndex];
			if (!countOfCcyOut.equals(0)) {
				subService = "CCYOUT";
				usageRecords.add(new UsageRecord(SERVICE_NAME, swiftAddress, subService, today, countOfCcyOut));
			}
		}

		return usageRecords;
	}

}
