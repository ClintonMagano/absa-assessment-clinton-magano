package com.psybergate.absa.assessment1.scenario1.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import com.psybergate.absa.assessment1.scenario1.entity.Transaction;
import com.psybergate.absa.assessment1.scenario1.entity.UsageRecord;

public interface UsageRecordExtractorService {

	void sendUsageRecordsDuringPeriod(LocalDate startDate, LocalDate endDate);

	Set<UsageRecord> getUsageRecords(List<Transaction> transactions);
	
}
