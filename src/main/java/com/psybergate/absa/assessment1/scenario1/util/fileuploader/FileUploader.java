package com.psybergate.absa.assessment1.scenario1.util.fileuploader;

public interface FileUploader {
	
	void uploadFileToBilling(String pathToFile);
	
}
