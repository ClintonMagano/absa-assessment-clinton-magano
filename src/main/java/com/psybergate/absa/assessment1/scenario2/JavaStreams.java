package com.psybergate.absa.assessment1.scenario2;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class JavaStreams {
	
	public static void main(String[] args) {
		filterStatuses();
	}

	private static void filterStatuses() {
		List<String> statuses = Arrays.asList("Received", "Pending", "","Awaiting Maturity", "Completed","");
		List<String> nonEmptyStatuses = statuses.stream()
			.filter(status -> !status.isEmpty())
			.collect(Collectors.toList());
		System.out.println(nonEmptyStatuses);
	}
	
}
